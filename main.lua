local N = component.fields().RefreshTime
local lastCommit = "" 

local function toQueue(Commits) 
   Commits = Commits:split('\n')
   for _,v in ipairs(Commits) do
      if v ~= "" then
         trace(v)
         iguana.log("We have a new commit: " ..v)
         queue.push{data=v}
         lastCommit = v
       end
   end 
end

-- TODO Global without a prefix
function runCommand(Command)
   local f = io.popen(Command)
   local out = f:read("*a")
   f:close()
   return out
end

function main(message)
   local Config = component.fields()
   local gitLastCommit  = 'cd '.. Config.WatchFolder ..' && git log -1 --pretty=format:"%H" '
   if lastCommit == "" then
	   lastCommit = runCommand(gitLastCommit)
      iguana.log("Start tracking commits after: " .. lastCommit)
   end
   
   local gitCmdPull = 'cd '.. Config.WatchFolder ..' && git pull'
   runCommand(gitCmdPull)
   
   local lastCommitAfterPull = runCommand(gitLastCommit)
   if lastCommitAfterPull == lastCommit then
      trace("nothing new")
      component.setTimer{data="", delay=1000*N}
      return
   end   
   
   local gitRevList = 'cd ' .. Config.WatchFolder .. ' && git rev-list --reverse --ancestry-path '
   local Commits = runCommand(gitRevList .. lastCommit .. ".." .. lastCommitAfterPull)
   trace(Commits)
   
	toQueue(Commits)
   -- TODO - make use of component status to indicate what this doing and/or if there is a problem
   -- with the configuration.
   
   -- TODO - If anything goes wrong we lose the timer.
   component.setTimer{data="", delay=1000*N}
end

main()
